<?php

/**
 * @file
 * Template.php - process theme data for your sub-theme.
 * 
 */


/**
 * Override or insert variables for the html template.
 */
/* -- Delete this line if you want to use this function
function red_feather_preprocess_html(&$vars) {
}
function red_feather_process_html(&$vars) {
}
// */


/**
 * Override or insert variables for the page templates.
 */
function red_feather_preprocess_page(&$vars) {
}
function red_feather_process_page(&$vars) {
}


/**
 * Override or insert variables into the node templates.
 */
function red_feather_preprocess_node(&$vars) {
//  drupal_set_message(t('<pre>@x</pre>', array('@x'=>var_export($vars, TRUE))));
}
function red_feather_process_node(&$vars) {
} 

/**
 * Override or insert variables into the entity templates.
 */
function red_feather_preprocess_entity(&$vars) {
  if ($vars['entity_type'] == 'profile2') {
    if ($vars['view_mode'] == 'panel') {
      $vars['page'] = TRUE;
    }
  }
}
function red_feather_process_entity(&$vars) {
} 


/**
 * Override or insert variables into the comment templates.
 */
/* -- Delete this line if you want to use these functions
function red_feather_preprocess_comment(&$vars) {
}
function red_feather_process_comment(&$vars) {
}
// */


/**
 * Override or insert variables into the block templates.
 */
/* -- Delete this line if you want to use these functions
function red_feather_preprocess_block(&$vars) {
}
function red_feather_process_block(&$vars) {
}
// */
